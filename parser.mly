%{
open Ast
open Lexing
open Parsing

(* Fonction de construction d'un [node_expr], qui renseigne correctement
		 la localisation de l'expression. *)
let mk_node e = (e, (Parsing.symbol_start_pos (), Parsing.symbol_end_pos ()))

(* Tentative pour la gestion des erreurs. Cependant Parsing.Parse_error ne prend pas d'argument, donc on ne peut pas faire comme Lexing.Lexical_error, et afficher directemet l'erreur ne fonctionne pas non plus *)
(* let parse_error str =
	let start = Parsing.symbol_start_pos ()
	and end_ = Parsing.symbol_end_pos ()
	in
		let line = string_of_int (start.pos_lnum)
			and cstart = string_of_int (start.pos_cnum - start.pos_bol)
			and cend = string_of_int (end_.pos_cnum - end_.pos_bol)
			in (Format.eprintf "File \"" ^ Sys.argv.(1) ^ "\", line " ^ line ^ ", characters " ^ cstart ^ "-" ^ cend ^ ":\n" ^ str) *)
%}

/* Déclaration des tokens */

%token <string> IDENT												// Noms variables
%token <int> INTEGER												// Entiers
%token <string> STRING												// Chaines de caracteres
%token EQ NEQ LT LE GT GE PLUS MINUS TIMES DIV PERCENT AND OR NOT	// == != < <= > >= + - * / % && || !
%token DOT LPAR RPAR THIS											// . ( ) this
%token INT BOOLEAN INSTANCEOF TRUE FALSE NULL						// int boolean instanceof true false null
%token NEW INCR DECR AFF											// new ++ -- =
%token RETURN FOR IF ELSE SEMCOL									// return for if else ;
%token LACC RACC													// { }
%token NATIVE VOID COMMA											// native void ,
%token PUBLIC CLASS STATIC MAIN BRACKETS							// public class static main String [] :: String est reconnu comme un ident
%token EXTENDS														// extends
%token EOF   														// Fin de fichier

/* tokens ajoutes pour les regles de priorite */
%token UMINUS
%token ACCESS
%token CAST

/* Déclaration des priorités */

/* ------------- Yaelle ------------- */
%nonassoc ACCESS THIS NEW
%nonassoc IF
%nonassoc ELSE
%nonassoc LPAR RPAR

%right AFF
%left OR
%left AND
%left EQ NEQ
%left LT GT GE LE INSTANCEOF
%left PLUS MINUS
%left TIMES DIV PERCENT
%right UMINUS NOT INCR DECR CAST
%left DOT

%nonassoc IDENT

/* --------------------------------- */

/* SYMBOLE DE DÉPART DE LA GRAMMAIRE : ON DONNE SON NOM ET SON TYPE
   (QUI SERA LE TYPE DES PROGRAMMES DANS L'AST)
*/

%start prog
%type <Ast.fichier> prog

%%

/* RÈGLES DE GRAMMAIRE */



/* Regles de grammaire des fichiers Deca. On precisera dans un commentaire pour chaque regle directement tiree de la grammaire sa definition dans la grammaire, et chaque regle de la grammaire sera suivie par les regles ajoutees qui lui sont liees */

/* ------------- Thibault ------------- */

/* <fichier> ::= <class_def>* <class_main> EOF */
prog:
	| class_defL class_main EOF { $1, $2 }
;


/* <class_def> ::= class <ident> <class_params>? (extends <class_expr>)? { <decl>* } */
class_def:
	| CLASS IDENT class_paramsOpt extendsOpt LACC declL RACC { $2, $3, $4, $6 }
;

class_defL:
	| /* empty */			{ [] }
	| class_def class_defL	{ $1 :: $2 }
;

extendsOpt:
	| /* empty */ 			{ None }
	| EXTENDS class_expr	{ Some $2 }
;


/* <class_main> ::= public class <ident> { public static void main (String <ident> []) <bloc> } */
class_main:
	| PUBLIC CLASS IDENT LACC PUBLIC STATIC VOID MAIN LPAR IDENT IDENT BRACKETS RPAR bloc RACC {
			(* $10 := deuxieme IDENT. Ce doit etre le String de la partie (String <ident> []), sinon erreur *)

			if ($10 = "String") then
				($3, $11, $14)			(* spec de class_main : ident * ident * bloc *)
			else
				raise (Parse_error)

	}
;


/* <class_params> ::= \< <ident>,+ \> */
class_params:
	| LT identL GT  { $2 }
;

identL:
	| IDENT					{ [$1] } /* Au moins un element */
	| IDENT COMMA identL	{ $1 :: $3 }
;

class_paramsOpt:
	| /* empty */	{ None }
	| class_params	{ Some $1 }
;


/* <class_expr> ::= <ident> (\< <class_expr>,+ \>)? */
class_expr:
	| IDENT						{ CEClass_expr ($1, []) }
	| IDENT LT class_exprL GT	{ CEClass_expr ($1, $3) }
;

class_exprL:
	| class_expr 					{ [$1] }
	| class_expr COMMA class_exprL	{ $1 :: $3 }
;


/* <decl> ::= <decl_att> | <decl_const> | <decl_meth> | <decl_native_meth> */
decl:
	| decl_att			{ DAtt ($1) }
	| decl_const		{ DConst ($1) }
	| decl_meth			{ DMeth ($1) }
	| decl_native_meth  { DNatMeth ($1) }
;

declL:
	| /* empty */	{ [] }
	| decl declL	{ $1 :: $2 }
;


/* <decl_att> ::= <type> <ident> ; (declaration d'attribut) */
decl_att:
	| type_ IDENT SEMCOL { $1, $2 }
;


/* <decl_const> ::= <ident> \( (<type> <ident>),* \) <bloc> */
decl_const:
	| IDENT LPAR type_identOpt RPAR bloc  { $1, $3, $5 }
;

/* Empeche de terminer sur une virgule ( empty COMMA [] ) */
type_identOpt:
	| /* empty */	{ [] }
	| type_identL	{ $1 }
;
/* (<type> <ident>)*, */
type_identL:
	| type_ IDENT					{ [($1, $2)] }
	| type_ IDENT COMMA type_identL	{ ($1, $2) :: $4 }
;


/* <decl_meth> ::= (<type> | void) <ident> \( (<type> <ident>),* \) <bloc> */
decl_meth:
	| VOID IDENT LPAR type_identOpt RPAR bloc	{ TMVoid, $2, $4, $6 }
	| type_ IDENT LPAR type_identOpt RPAR bloc	{ TMType ($1), $2, $4, $6 }
;


/* <decl_native_meth> ::= native (<type> | void) <ident> \( (<type> <ident>),* \) ; */
decl_native_meth:
	| NATIVE type_ IDENT LPAR type_identOpt RPAR SEMCOL	{ TMType ($2), $3, $5 }
	| NATIVE VOID IDENT LPAR type_identOpt RPAR SEMCOL	{ TMVoid, $3, $5 }
;


/* <type> ::= boolean | int | <class_expr> */
type_:
	| BOOLEAN		{ TBool }
	| INT			{ TInt }
	| class_expr	{ TClass ($1) }
;

/* --------------------------------- */

/* Regles de la grammaire des expressions et des instructions */

/* ----------------- Yaelle & Thibault ---------------- */


bloc:
	| LACC instrL RACC { $2 }	/* { <instr>* } */
;

instrL:
	| /* empty */	{ [] }
	| instr instrL	{ $1 :: $2}
;


instr:
	| SEMCOL
		{ IEmpty }
	| instr_expr SEMCOL
		{ IInstr_expr ($1) }

/* <type> <ident> (= <expr>)?; */
	| type_ IDENT
		{ IDec_init ($1, $2, None) }
	| type_ IDENT AFF expr
		{ IDec_init ($1, $2, Some $4) }

/* if \( <expr \) <instr> (else <instr>)? */
	| IF LPAR expr RPAR instr %prec IF
		{ IIf ($3, $5) }
	| IF LPAR expr RPAR instr ELSE instr
		{ IIf_else ($3, $5, $7) }

/* for \( <expr>?; <expr>?; <expr>? \) <instr> */
	| FOR LPAR exprOpt SEMCOL exprOpt SEMCOL exprOpt RPAR instr
		{ IFor ($3, $5, $7, $9) }

	| bloc
		{ IBloc ($1) }
	| RETURN exprOpt SEMCOL
		{ IReturn ($2) }
;


instr_expr:
	| access AFF expr	/* <acces> = <expr> */		{ IEAssign ($1, $3) }
	| call	/* <appel> */							{ IECall ($1) }
	| INCR access	/* ++ <access> */				{ IELeft (IOIncr, $2) }
	| DECR access	/* -- <access> */				{ IELeft (IODecr, $2) }
	| access INCR	/* <access> ++ */				{ IERight (IOIncr, $1) }
	| access DECR	/* <access> -- */				{ IERight (IODecr, $1) }
	| NEW class_expr LPAR expr_listOpt RPAR	/* new <class_expr> \( <expr>,* \) */
													{ IENew ($2, $4) }
;


/* <acces> \( <expr_list>? \) */
call:
	| access LPAR expr_list RPAR	{ CAcc ($1, $3) }
;


expr:

/* true | false | <entier > | <chaine> | null */
	| value								{ mk_node (EVal ($1)) }

/* Operations unaires */
	| NOT expr							{ mk_node (EUop(UNot, $2)) }
	| MINUS expr %prec UMINUS			{ mk_node (EUop(UMinus, $2)) }

/* Operations arithmetiques */
	| expr PLUS expr					{ mk_node (EOp (OAop (Add), $1, $3)) }
	| expr MINUS expr					{ mk_node (EOp (OAop (Sub), $1, $3)) }
	| expr DIV expr						{ mk_node (EOp (OAop (Div), $1, $3)) }
	| expr TIMES expr					{ mk_node (EOp (OAop (Mul), $1, $3)) }
	| expr PERCENT expr					{ mk_node (EOp (OAop (Mod), $1, $3)) }

/* Operations donnant un booleen */
	| expr EQ expr						{ mk_node (EOp (OBop (Eq), $1, $3)) }
	| expr NEQ expr						{ mk_node (EOp (OBop (Neq), $1, $3)) }
	| expr LE expr						{ mk_node (EOp (OBop (Le), $1, $3)) }
	| expr GE expr						{ mk_node (EOp (OBop (Ge), $1, $3)) }
	| expr LT expr						{ mk_node (EOp (OBop (Lt), $1, $3)) }
	| expr GT expr						{ mk_node (EOp (OBop (Gt), $1, $3)) }
	| expr AND expr						{ mk_node (EOp (OBop (And), $1, $3)) }
	| expr OR expr						{ mk_node (EOp (OBop (Or), $1, $3)) }

/* instance of */
	| expr INSTANCEOF class_expr		{ mk_node (EInst_of ($1,$3)) }

/* cast */
	| LPAR expr RPAR expr %prec CAST	{ mk_node (ECast (CExpr ($2), $4)) }
	| LPAR INT RPAR expr %prec CAST		{ mk_node (ECast (CInt, $4)) }
	| LPAR BOOLEAN RPAR expr %prec CAST	{ mk_node (ECast (CBool, $4)) }

/* <instr_expr>, <acces>, et \( <expr> \) */
	| instr_expr						{ mk_node (EInstr_expr ($1)) }
	| access	%prec ACCESS			{ mk_node (EAcc ($1)) }
	| LPAR expr RPAR					{ mk_node (EPar_expr ($2)) }
;

exprOpt:
	| /* empty */	{ None }
	| expr			{ Some $1 }
;


access:
	| THIS							{ This }
	| IDENT							{ AId ($1) }
	| call DOT IDENT				{ ACall ($1, $3) }
	| access DOT IDENT				{ AAcc ($1, $3) }
	| LPAR expr RPAR DOT IDENT		{ APar_exp ($2, $5) }
;


value:
	| INTEGER	{ VInt (Int32.of_int $1) }
	| TRUE		{ VBool (true) }
	| FALSE		{ VBool (false) }
	| STRING	{ VStr ($1) }
	| NULL		{ VNull }
;


expr_list:
	| expr					{ [$1] }
	| expr COMMA expr_list	{ $1 :: $3 }
;

/* Permet de gerer les listes vides */
expr_listOpt:
	| /* empty */	{ [] }
	| expr_list		{ $1 }
;

/* --------------------------------- */
