{
  (* Analyse lexicale *)

	open Lexing
	open Parser

  (* Déclaration d'une exception *)
	exception Lexical_error of string

	let keywords = Hashtbl.create 97	
	let () = List.iter (fun (s,t) -> Hashtbl.add keywords s t)
			["new", NEW; "this", THIS; "int", INT; "boolean", BOOLEAN; "instanceof", INSTANCEOF;
			"true", TRUE; "false", FALSE; "null", NULL; "return", RETURN; "for", FOR; "if", IF;
			"else", ELSE; "native", NATIVE; "void", VOID; "public", PUBLIC; "class", CLASS;
			"static", STATIC; "main", MAIN; "extends", EXTENDS]

	(* gestion d'erreur *)
	let err str buf =
		let start = Lexing.lexeme_start_p buf
		and end_ = Lexing.lexeme_end_p buf
		in
			let line = string_of_int (start.pos_lnum)
			and cstart = string_of_int (start.pos_cnum - start.pos_bol)
			and cend = string_of_int (end_.pos_cnum - end_.pos_bol)
			in ("File \"" ^ Sys.argv.(1) ^ "\", line " ^ line ^ ", characters " ^ cstart ^ "-" ^ cend ^ ":\n" ^ str)
	;;

}

(* EXPRESSION RÉGULIÈRE POUR LES LETTRES *)

let alpha = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let str = '"' [^'"']* '"'
let ident = (alpha | '_')(alpha | '_' | digit)*

(* EXPRESSION RÉGULIÈRE POUR LES CHIFFRES *)


(* RÈGLES RÉCURSIVES *)

rule token = parse
	| [' ' '\t' '\n']	{ token lexbuf }


     (* PLACER TOUS LES AUTRES CAS ICI *)

	| ident as s {
					try Hashtbl.find keywords s with Not_found -> IDENT(s)
				 }

	| digit+ as n {
					INTEGER (Int32.to_int(Int32.of_string n))
                	}

	| str			{let s = Lexing.lexeme lexbuf in
							let str = String.sub s 1 (String.length(s)-2)
							and f a =
								let c = Char.code a in
									if (c < 32 || c > 126) then
										let error = err "Caractere invalide dans la chaine" lexbuf
										in raise (Lexical_error error)
									else ()
							in String.iter f str; STRING(str)
					}

	| "*/"	{ let error = err "Fin de commentaire non associee au debut d'un commentaire" lexbuf
			  in raise (Lexical_error error)
			}

	| "++"				{ INCR }
	| "--"				{ DECR }
	| '='				{ AFF }

	| "=="				{ EQ }
	| "!="				{ NEQ }
	| "<="				{ LE }
	| ">="				{ GE }
	| '<'				{ LT }
	| '>'				{ GT }

	| '+'				{ PLUS }
	| '-'				{ MINUS }
	| '*'				{ TIMES }
	| '/'				{ DIV }
	| '%'				{ PERCENT }

	| "&&"				{ AND }
	| "||"				{ OR }
	| '!'				{ NOT }

	| '.'				{ DOT }
	| ','				{ COMMA }
	| ';'				{ SEMCOL }

	| '('				{ LPAR }
	| ')'				{ RPAR }
	| '{'				{ LACC }
	| '}'				{ RACC }
	| "[]"				{ BRACKETS }

	| "//"				{ commentL lexbuf }
	| "/*"				{ commentP lexbuf }

	| eof				{ EOF }
	| _
						{
							(* Quelque chose d'inconnu *)
							let error = err "Caractere invalide" lexbuf
							in raise (Lexical_error error)
						}

and commentL = parse
	| '\n'		{ token lexbuf }
	| eof		{ EOF }
	| _			{ commentL lexbuf }

and commentP = parse
	| "*/"		{ token lexbuf }
	| eof		{
					let error = err "Pas de fin de commentaire" lexbuf
					in raise (Lexical_error error)
				}
	| _			{ commentP lexbuf }

