(* Author : VINCONT Yaelle && FERET Thibault *)

(* Identificateur
--------------------------------------------------------- *)
type ident = string;;


(* Sous types
---------------------------------------------- *)
type position =  Lexing.position * Lexing.position

type aop = (* arithmetic operations *)
    Add
  | Sub
  | Mul
  | Div
  | Mod
;;

type bop = (* boolean operation *)
    Eq
  | Neq
  | Lt       (* Strictement inf *)
  | Le       (* Inf ou egal *)
  | Gt       (* Strictement sup *)
  | Ge       (* Sup ou egal *)
  | And
  | Or
;;

type uop = UNot | UMinus;; (* unitary operator *)

(* On met un V devant les noms des constructeurs pour signifier qu'il s'agit des constructeurs des valeurs *)
type value = VInt of int32 | VBool of bool | VStr of string | VNull;;

type incr_op = IOIncr | IODecr

type node_expr = expr * position

(* On met un C devant les noms des constructeurs pour signifier qu'il s'agit des constructeurs des casteurs *)
and cast = CExpr of node_expr | CInt | CBool

(* Types de la grammaire : instructions et expressions
--------------------------------------------------------- *)
and operator = OAop of aop | OBop of bop

(* On met un E devant les noms des constructeurs pour signifier qu'il s'agit des constructeurs de l'expression *)
and access =
    This
  | AId of ident
  | ACall of call * ident
  | AAcc of access * ident
  | APar_exp of node_expr * ident
and
expr =
    EVal of value
  | EUop of uop * node_expr
  | EOp of operator * node_expr * node_expr
  | EInst_of of node_expr * class_expr
  | ECast of cast * node_expr
  | EInstr_expr of instr_expr
  | EAcc of access
  | EPar_expr of node_expr
and
call = CAcc of access * node_expr list
and
instr_expr =
    IEAssign of access * node_expr
  | IECall of call
  | IELeft of incr_op * access
  | IERight of incr_op * access
  | IENew of class_expr * node_expr list
and
instr =
    IEmpty
  | IInstr_expr of instr_expr
  | IDec_init of type_ * ident * node_expr option (* declaration ac init optionnelle *)
  | IIf of node_expr * instr
  | IIf_else of node_expr * instr * instr
  | IFor of node_expr option * node_expr option * node_expr option * instr
  | IBloc of instr list
  | IReturn of node_expr option

(* Types de la grammaire : fichiers sources
--------------------------------------------------------- *)

and
(* <ident> ( <class_expr +,> )? *)
class_expr = CEClass_expr of ident * class_expr list
and type_ = TBool | TInt | TClass of class_expr
;;

(* definition des types de declaration *)

type decl_att = type_ * ident (* attribut *)
type decl_const = ident * (type_ * ident) list * instr list (* constructeur *)
type type_meth = TMType of type_ | TMVoid (* a voir *)
type decl_meth = type_meth * ident * (type_ * ident) list * instr list (* methode ac instr list := block *)
type decl_native_meth = type_meth * ident * (type_ * ident) list (* native (<type> | void) <ident> ((<type><ident>)*,) *)

type decl =
  DAtt of decl_att
  | DConst of decl_const
  | DMeth of decl_meth
  | DNatMeth of decl_native_meth
;;

type class_params = ident list (* \< <ident>+, \> *)

(* public class <ident> { public static void main(String <ident> []) <bloc> with bloc := instr list *)
type class_main = ident * ident * instr list

(* class <ident> <class_params>? (extends <class_expr>)?{<decl>*} *)
type class_def = ident * class_params option * class_expr option * decl list (* class_expr option := extends *)

type fichier = class_def list * class_main (* <class_def>* <class_main> EOF *)
