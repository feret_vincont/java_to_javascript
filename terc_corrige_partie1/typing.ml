open Ast
(* author : Yaëlle VINÇONT *)


       
let rec type_expr env e =
  match e.node with
  | Econst (Cint _) -> Tint
  | Econst (Cbool _) -> Tboolean
  | Econst (Cnull) -> Tnull
  | Econst (Cstring _) -> Type_class.t_String

  | Eunop (op, e) ->
     let te = type_expr env e in
     begin
       match op with
       | Uneg ->
	  if (te <> Tint) then
	    failwith "erreur de type"
	  else
	    Tint
       | Unot ->
	  if (te <> Tboolean) then
	    failwith "erreur de type"
	  else
	    Tboolean
       | _ -> failwith "todo Eunop"
     end

  | Ebinop (e1, op, e2) ->
     let te1 = type_expr env e1
     and te2 = type_expr env e2
     in
     begin
       match op with
       (*     | Beq | Bneq ->
	if !(Type_class.compatible t1 t2) then
	  failwith "erreur de type"
	else
	  Tboolean *)
       | Blt | Blte | Bgt | Bgte ->
			     if (te1 <> Tint) || (te2 <> Tint) then
			       failwith "erreur de type"
			     else
			       Tboolean
       | Bsub | Bmul | Bdiv | Bmod ->
			       if (te1 <> Tint) || (te2 <> Tint) then
				 failwith "erreur de type"
			       else
				 Tint
       | Band | Bor ->
		 if (te1 <> Tboolean) || (te2 <> Tboolean) then
		   failwith "erreur de type"
		 else
		   Tboolean
       | Badd ->
	  begin
	    match te1, te2 with
	    | Tint, Tint -> Tint
	    | Tint, Tclass t
	    | Tclass t, Tint ->
	       if (t.name <> "String") then
		 failwith "erreur de type"
	       else
		 Type_class.t_String
	    | Tclass t1, Tclass t2 ->
	       if (t1.name <> "String" || t2.name <> "String") then
		 failwith "erreur de type"
	       else
		 Type_class.t_String
	    | _ -> failwith "erreur de type"
	  end
       | _ -> failwith "todo binop" 
     end
       
  | _ -> failwith "todo type_expr"


let rec type_instr env i =
  match i.node with
  | Iexpr e ->
     let _ = type_expr env e in
     env
       
  | Iif (e, i1, i2) ->
     let te = type_expr env e in
     if (te <> Tboolean) then
       failwith "erreur de type"
     else
       let _ = type_instr env i1 in
       let _ = type_instr env i2 in
       env
	 
  | Iblock [] -> env
  | Iblock [ i ] ->
     let _ = type_instr env i in
     env

  | Ireturn (None) ->
     env
  | Ireturn (Some e) ->
     let _ = type_expr env e in
     env
		   
  | _ -> failwith "todo type_instr"



let type_prog prog =

  let _class_list, _main_class, main_body = prog
  in
  (* faire le tri topo de _class_list et construire DELTA *)

  type_instr [] main_body
