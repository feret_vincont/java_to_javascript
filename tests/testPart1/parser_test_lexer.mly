%{
open Ast
open Parsing

%}

/* Déclaration des tokens */

%token <string> IDENT							                        // Noms variables
%token <int> INTEGER													// Entiers
%token <string> STRING														// Chaines de caracteres
%token EQ NEQ LT LE GT GE PLUS MINUS TIMES DIV PERCENT AND OR NOT		// == != < <= > >= + - * / % && || ! 
%token DOT LPAR RPAR THIS						                               // . ( ) this
%token INT BOOLEAN INSTANCEOF TRUE FALSE NULL				               // int boolean instanceof true false null
%token NEW INCR DECR AFF						                               // new ++ -- =
%token RETURN FOR IF ELSE SEMCOL					                         // return for if else ;
%token LACC RACC							                                     // { }
%token NATIVE VOID COMMA						                               // native void ,
%token PUBLIC CLASS STATIC MAIN BRACKETS		              	 // public class static main String [] :: String est reconnu comme un ident
%token EXTENDS								                                     // extends
%token EOF   							                                         // Fin de fichier

/* Déclaration des priorités */

/* SYMBOLE DE DÉPART DE LA GRAMMAIRE : ON DONNE SON NOM ET SON TYPE
   (QUI SERA LE TYPE DES PROGRAMMES DANS L'AST)

  IL FAUT REMPLACER 'unit' CI-DESSOUS PAR LE TYPE DE L'AST.
*/

%start prog
%type < unit > prog

%%

/* RÈGLES DE GRAMMAIRE */

/* Test pour verifier un entier : */
prog:
    | INTEGER EOF		{ print_int ($1); print_string ("Le lexer a marche"); }
    | INSTANCEOF EOF	{ print_string ("instanceof"); print_string ("Le lexer a marche"); }
    | TRUE EOF			{ print_string ("true"); print_string ("Le lexer a marche"); }
    | IDENT EOF			{ print_string ($1); print_string ("Le lexer a marche"); }
    | STRING EOF		{ print_string ($1); print_string ("Le lexer a marche"); }
    | LT EOF			{ print_string ("<"); print_string ("Le lexer a marche"); }
;

